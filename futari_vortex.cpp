/*
 *  
 *  
 *  _________ ____  .-. _________/ ____ .-. ____ 
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'      
 *  
 *  
 *  art & game engine
 *  
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *  
 * 
 *  This file is part of PROJECT_NAME
 *  For the latest info, see http://polymorph.cool/
 *  
 *  Copyright (c) 2019 polymorph.cool
 *  
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *  
 *  ___________________________________( ^3^)_____________________________________
 *  
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *  
 * 
 */

/* 
 * File:   futari_vortex.cpp
 * Author: frankiezafe
 * 
 * Created on January 11, 2019, 6:23 PM
 */

#include "futari_vortex.h"

FutariVortex::FutariVortex() :
up(0, 1, 0) {
    range = FUTARI_VORTEX_MINRANGE;
    FutariVortex::_data.ptr = this;
    FutariVortex::_data.futari_type = FutariModifierData::FMT_VORTEX;
}

void FutariVortex::set_range(real_t r) {
    if (r < FUTARI_VORTEX_MINRANGE) {
        range = FUTARI_VORTEX_MINRANGE;
        return;
    }
    range = r;
#ifdef TOOLS_ENABLED
    update_gizmo();
#endif
}

void FutariVortex::set_height(real_t r) {
    if (r < FUTARI_VORTEX_MINHEIGHT) {
        height = FUTARI_VORTEX_MINHEIGHT;
        return;
    }
    height = r;
#ifdef TOOLS_ENABLED
    update_gizmo();
#endif
}

real_t FutariVortex::get_height() const {
    return height;
}

void FutariVortex::set_top_texture(const Ref<GradientTexture> &tex) {
    top_texture = tex;
}

Ref<GradientTexture> FutariVortex::get_top_texture() const {
    return top_texture;
}

void FutariVortex::set_center_texture(const Ref<GradientTexture> &tex) {
    strength_decay_texture = tex;
}

Ref<GradientTexture> FutariVortex::get_center_texture() const {
    return strength_decay_texture;
}

void FutariVortex::set_bottom_texture(const Ref<GradientTexture> &tex) {
    bottom_texture = tex;
}

Ref<GradientTexture> FutariVortex::get_bottom_texture() const {
    return bottom_texture;
}

void FutariVortex::set_height_texture(const Ref<GradientTexture> &tex) {
    height_texture = tex;
}

Ref<GradientTexture> FutariVortex::get_height_texture() const {
    return height_texture;
}

void FutariVortex::refresh() {

    FutariVortex::_data.reset();
    
    if ( !enabled ) {
        FutariVortex::_data.changed_enabled = FutariVortex::_data.enabled != enabled;
        FutariVortex::_data.changed = FutariVortex::_data.changed_enabled;
        FutariVortex::_data.enabled = enabled;
        return;
    }

    Vector3 p = get_global_transform().origin;
    Vector3 o = get_transform().basis.xform(up);
    float s = strength;
    if (!is_visible()) {
        s *= 0;
    }
    
    FutariVortex::_data.changed_layer = FutariVortex::_data.futari_layers != futari_layers;
    FutariVortex::_data.changed_enabled = FutariVortex::_data.enabled != enabled;
    FutariVortex::_data.changed_position = FutariVortex::_data.position != p;
    FutariVortex::_data.changed_orientation = FutariVortex::_data.orientation != o;
    FutariVortex::_data.changed_strength = FutariVortex::_data.strength != s;
    FutariVortex::_data.changed_strength_decay = 
            FutariVortex::_data.strength_decay != strength_decay_texture || 
            FutariVortex::_data.strength_decay.is_valid() != strength_decay_texture.is_valid();
    FutariVortex::_data.changed_lifetime = 
            FutariVortex::_data.lifetime != lifetime_texture || 
            FutariVortex::_data.lifetime.is_valid() != lifetime_texture.is_valid();
    FutariVortex::_data.changed_range = FutariVortex::_data.range != range;
    FutariVortex::_data.changed_height = FutariVortex::_data.height != height;
    FutariVortex::_data.changed_velocity_mult = FutariVortex::_data.velocity_mult != velocity_mult;
    FutariVortex::_data.changed_position_mult = FutariVortex::_data.position_mult != position_mult;

    FutariVortex::_data.changed_top_tex = 
            FutariVortex::_data.top_tex != top_texture || 
            FutariVortex::_data.top_tex.is_valid() != top_texture.is_valid();
    FutariVortex::_data.changed_bottom_tex = 
            FutariVortex::_data.bottom_tex != bottom_texture || 
            FutariVortex::_data.bottom_tex.is_valid() != bottom_texture.is_valid();
    FutariVortex::_data.changed_height_tex = 
            FutariVortex::_data.height_tex != height_texture || 
            FutariVortex::_data.height_tex.is_valid() != height_texture.is_valid();

    FutariVortex::_data.changed = 
            FutariVortex::_data.changed_layer ||
            FutariVortex::_data.changed_enabled ||
            FutariVortex::_data.changed_position ||
            FutariVortex::_data.changed_orientation ||
            FutariVortex::_data.changed_strength ||
            FutariVortex::_data.changed_strength_decay ||
            FutariVortex::_data.changed_lifetime ||
            FutariVortex::_data.changed_range ||
            FutariVortex::_data.changed_height ||
            FutariVortex::_data.changed_top_tex ||
            FutariVortex::_data.changed_bottom_tex ||
            FutariVortex::_data.changed_height_tex ||
            FutariVortex::_data.changed_velocity_mult ||
            FutariVortex::_data.changed_position_mult;


    if (FutariVortex::_data.changed) {
        FutariVortex::_data.futari_layers = futari_layers;
        FutariVortex::_data.enabled = enabled;
        FutariVortex::_data.position = p;
        FutariVortex::_data.orientation = o;
        FutariVortex::_data.strength = s;
        FutariVortex::_data.strength_decay = strength_decay_texture;
        FutariVortex::_data.lifetime = lifetime_texture;
        FutariVortex::_data.range = range;
        FutariVortex::_data.height = height;
        FutariVortex::_data.top_tex = top_texture;
        FutariVortex::_data.bottom_tex = bottom_texture;
        FutariVortex::_data.height_tex = height_texture;
        FutariVortex::_data.velocity_mult = velocity_mult;
        FutariVortex::_data.position_mult = position_mult;
    }

}

FutariModifierData* FutariVortex::data_ptr() {
    return (FutariModifierData*) &(FutariVortex::_data);
}

void FutariVortex::_bind_methods() {

    ClassDB::bind_method(D_METHOD("set_height", "height"), &FutariVortex::set_height);
    ClassDB::bind_method(D_METHOD("get_height"), &FutariVortex::get_height);

    ClassDB::bind_method(D_METHOD("set_top_texture", "top_tex"), &FutariVortex::set_top_texture);
    ClassDB::bind_method(D_METHOD("get_top_texture"), &FutariVortex::get_top_texture);

    ClassDB::bind_method(D_METHOD("set_center_texture", "center_tex"), &FutariVortex::set_center_texture);
    ClassDB::bind_method(D_METHOD("get_center_texture"), &FutariVortex::get_center_texture);

    ClassDB::bind_method(D_METHOD("set_bottom_texture", "bottom_tex"), &FutariVortex::set_bottom_texture);
    ClassDB::bind_method(D_METHOD("get_bottom_texture"), &FutariVortex::get_bottom_texture);

    ClassDB::bind_method(D_METHOD("set_height_texture", "height_tex"), &FutariVortex::set_height_texture);
    ClassDB::bind_method(D_METHOD("get_height_texture"), &FutariVortex::get_height_texture);

    String prop;

    ADD_GROUP("Vortex", "vortex_");
    ADD_PROPERTY(PropertyInfo(Variant::BOOL, "enabled"), "set_enabled", "get_enabled");
    prop = String::num_real(FUTARI_VORTEX_MINRANGE) + ",100,0.01,or_greater";
    ADD_PROPERTY(PropertyInfo(Variant::REAL, "range", PROPERTY_HINT_RANGE, prop), "set_range", "get_range");
    prop = String::num_real(FUTARI_VORTEX_MINHEIGHT) + ",100,0.01,or_greater";
    ADD_PROPERTY(PropertyInfo(Variant::REAL, "height", PROPERTY_HINT_RANGE, prop), "set_height", "get_height");
    ADD_PROPERTY(PropertyInfo(Variant::REAL, "strength", PROPERTY_HINT_RANGE, "-100,100,0.01,or_greater"), "set_strength", "get_strength");
    ADD_PROPERTY(PropertyInfo(Variant::OBJECT, "lifetime_texture", PROPERTY_HINT_RESOURCE_TYPE, "GradientTexture"), "set_lifetime_texture", "get_lifetime_texture");
    ADD_PROPERTY(PropertyInfo(Variant::REAL, "velocity_mult", PROPERTY_HINT_RANGE, "0,1"), "set_velocity_mult", "get_velocity_mult");
    ADD_PROPERTY(PropertyInfo(Variant::REAL, "position_mult", PROPERTY_HINT_RANGE, "0,1"), "set_position_mult", "get_position_mult");

    ADD_GROUP("Controls", "vortex_");
    ADD_PROPERTY(PropertyInfo(Variant::OBJECT, "top_tex", PROPERTY_HINT_RESOURCE_TYPE, "GradientTexture"), "set_top_texture", "get_top_texture");
    ADD_PROPERTY(PropertyInfo(Variant::OBJECT, "center_tex", PROPERTY_HINT_RESOURCE_TYPE, "GradientTexture"), "set_center_texture", "get_center_texture");
    ADD_PROPERTY(PropertyInfo(Variant::OBJECT, "bottom_tex", PROPERTY_HINT_RESOURCE_TYPE, "GradientTexture"), "set_bottom_texture", "get_bottom_texture");
    ADD_PROPERTY(PropertyInfo(Variant::OBJECT, "height_tex", PROPERTY_HINT_RESOURCE_TYPE, "GradientTexture"), "set_height_texture", "get_height_texture");

}
