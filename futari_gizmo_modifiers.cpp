/*
 *  
 *  
 *  _________ ____  .-. _________/ ____ .-. ____ 
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'      
 *  
 *  
 *  art & game engine
 *  
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *  
 * 
 *  This file is part of PROJECT_NAME
 *  For the latest info, see http://polymorph.cool/
 *  
 *  Copyright (c) 2019 polymorph.cool
 *  
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *  
 *  ___________________________________( ^3^)_____________________________________
 *  
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *  
 * 
 */

/* 
 * File:   futari_gizmo_modifiers.cpp
 * Author: frankiezafe
 * 
 * Created on January 19, 2019, 10:09 AM
 */

#include "futari_gizmo_modifiers.h"

// +++++++++++++++ MODIFIER +++++++++++++++

FutariModifierGizmoPlugin::FutariModifierGizmoPlugin() {
    Color gizmo_color = EDITOR_DEF(FUTARI_DISABLED_COLOR_NAME, Color(FUTARI_DISABLED_COLOR));
    create_material(FUTARI_DISABLED_MATERIAL, gizmo_color);
}

bool FutariModifierGizmoPlugin::has_gizmo(Spatial *p_spatial) {
    return Object::cast_to<FutariModifier>(p_spatial);
}

String FutariModifierGizmoPlugin::get_name() const {
    return "FutariModifier";
}

bool FutariModifierGizmoPlugin::is_selectable_when_hidden() const {
    return true;
}

void FutariModifierGizmoPlugin::redraw(EditorSpatialGizmo *p_gizmo) {
}

String FutariModifierGizmoPlugin::get_handle_name(const EditorSpatialGizmo *p_gizmo, int p_idx) const {
    if (p_idx == 0)
        return "Range";
    else
        return "Strength";
}

Variant FutariModifierGizmoPlugin::get_handle_value(EditorSpatialGizmo *p_gizmo, int p_idx) const {

    FutariModifier *mod = Object::cast_to<FutariModifier>(p_gizmo->get_spatial_node());
    if (p_idx == 0)
        return mod->get_range();
    if (p_idx == 1)
        return mod->get_strength();
    return Variant();

}

void FutariModifierGizmoPlugin::set_handle(EditorSpatialGizmo *p_gizmo, int p_idx, Camera *p_camera, const Point2 &p_point) {

    //    FutariModifier *mod = Object::cast_to<FutariModifier>(p_gizmo->get_spatial_node());
    //    Transform gt = mod->get_global_transform();
    //    gt.orthonormalize();
    //    Transform gi = gt.affine_inverse();

    //    Vector3 ray_from = p_camera->project_ray_origin(p_point);
    //    Vector3 ray_dir = p_camera->project_ray_normal(p_point);

    //    Vector3 s[2] = {gi.xform(ray_from), gi.xform(ray_from + ray_dir * 4096)};

    //    if (p_idx == 0) {
    //
    //    } else if (p_idx == 1) {
    //
    //    }

}

void FutariModifierGizmoPlugin::commit_handle(EditorSpatialGizmo *p_gizmo, int p_idx, const Variant &p_restore, bool p_cancel) {

    FutariModifier *mod = Object::cast_to<FutariModifier>(p_gizmo->get_spatial_node());

    if (p_cancel) {

        if (p_idx == 0) {
            mod->set_range(p_restore);
        } else if (p_idx == 0) {
            mod->set_strength(p_restore);
        }

    } else if (p_idx == 0) {

        //        UndoRedo *ur = SpatialEditor::get_singleton()->get_undo_redo();
        //        ur->create_action(TTR("Change Light Radius"));
        //        ur->add_do_method(light, "set_param", Light::PARAM_RANGE, light->get_param(Light::PARAM_RANGE));
        //        ur->add_undo_method(light, "set_param", Light::PARAM_RANGE, p_restore);
        //        ur->commit_action();

    } else if (p_idx == 1) {

        //        UndoRedo *ur = SpatialEditor::get_singleton()->get_undo_redo();
        //        ur->create_action(TTR("Change Light Radius"));
        //        ur->add_do_method(light, "set_param", Light::PARAM_SPOT_ANGLE, light->get_param(Light::PARAM_SPOT_ANGLE));
        //        ur->add_undo_method(light, "set_param", Light::PARAM_SPOT_ANGLE, p_restore);
        //        ur->commit_action();

    }
}

void FutariModifierGizmoPlugin::curved_arrow(
        const real_t radius,
        const real_t length,
        const real_t thickness,
        const real_t radian,
        Vector<Vector3>& arr) {

    const int arrow_points = 5;
    Vector3 arrow[arrow_points] = {
        Vector3(0, -0.3, 0),
        Vector3(0, -0.8, 0),
        Vector3(-1, 0, 0),
        Vector3(0, 0.8, 0),
        Vector3(0, 0.3, 0),
    };

    // creation of 2d quarter of gizmo
    Vector<Vector3> arrow_dots;

    //    float mult = (abs(strength) <= 1) ? 1 : sqrt(abs(strength));

    float str = length;
    if (length > 0) {
        str = std::min(length, radius - thickness);
        if (str < 0) {
            str = 0;
        }
    }
    // angle of the arrow basis intersection with circle
    float m = thickness;
    float proja = 0;
    float arrow_offset = 0;
    float ratio = thickness * arrow[4].y / radius;
    if (ratio > 0.9999) {
        ratio = 0.9999;
    }
    proja = asin(ratio);
    if (length < 0 && proja > 20 / 180.f * M_PI) {
        proja = 20 / 180.f * M_PI;
        m = abs(sin(proja)) * radius / arrow[4].y;
        arrow_offset = 1 - cos(proja);
    }
    arr.push_back(Vector3(
            radius - arrow_offset * radius,
            arrow[0].y * m,
            0));
    float multx = 1;
    if (length < 0) {
        multx *= -1;
    }
    for (int i = 0; i < 5; ++i) {
        arr.push_back(Vector3(
                radius - (str + arrow_offset * radius) + arrow[i].x * m * multx,
                arrow[i].y * m,
                0));
    }
    arr.push_back(Vector3(
            radius - arrow_offset * radius,
            arrow[4].y * m,
            0));
    // arc
    float a = proja;
    int segm = int( FUTARI_CIRCLE_DEFINITION * (radian / M_PI * 2));
    float agap = (radian - proja * 2) / segm;
    for (int i = 0; i < segm + 1; ++i) {
        arr.push_back(Vector3(cos(a) * radius, sin(a) * radius, 0));
        a += agap;
    }

}

void FutariModifierGizmoPlugin::circle(const real_t radius, Vector<Vector3>& c) {

    float a = 0;
    float agap = M_PI * 2 / FUTARI_CIRCLE_DEFINITION;
    for (int i = 0; i < FUTARI_CIRCLE_DEFINITION; ++i) {
        c.push_back(Vector3(cos(a) * radius, sin(a) * radius, 0));
        a += agap;
    }

}

// +++++++++++++++ WIND +++++++++++++++

FutariWindGizmoPlugin::FutariWindGizmoPlugin() {

    Color gizmo_color = EDITOR_DEF(FUTARI_WIND_COLOR_NAME, Color(FUTARI_WIND_COLOR));
    create_material(FUTARI_WIND_MATERIAL, gizmo_color);
    create_handle_material(FUTARI_HANDLE_MATERIAL);

}

bool FutariWindGizmoPlugin::has_gizmo(Spatial *p_spatial) {
    return Object::cast_to<FutariWind>(p_spatial);
}

String FutariWindGizmoPlugin::get_name() const {
    return "FutariWind";
}

void FutariWindGizmoPlugin::redraw(EditorSpatialGizmo *p_gizmo) {

    FutariWind *wind = Object::cast_to<FutariWind>(p_gizmo->get_spatial_node());

    p_gizmo->clear();

    Ref<Material> material;
    if (wind->get_enabled()) {
        material = get_material(FUTARI_WIND_MATERIAL, p_gizmo);
    } else {
        material = get_material(FUTARI_DISABLED_MATERIAL, p_gizmo);
    }
    Ref<Material> handles_material = get_material(FUTARI_HANDLE_MATERIAL);

    Vector<Vector3> arrow_dots;
    float strength = wind->get_strength();
    curved_arrow(
            wind->get_range(),
            -strength,
            (abs(strength) <= 1) ? 1 : sqrt(abs(strength)),
            M_PI * 2,
            arrow_dots);

    int alnum = arrow_dots.size();

    Vector<Vector3> circle_dots;
    circle(wind->get_range(), circle_dots);

    Vector<Vector3> lines;

    for (int r = 0; r < 3; ++r) {
        Basis matrix;
        switch (r) {
            case 0:
                matrix.rotate(Vector3(0, 0, 1), M_PI / 2);
            case 1:
                matrix.rotate(matrix.xform(Vector3(0, 1, 0)), M_PI / 2);
                for (int i = 0, j = 1; i < alnum - 1; ++i, ++j) {
                    j %= alnum;
                    lines.push_back(matrix.xform(arrow_dots[i]));
                    lines.push_back(matrix.xform(arrow_dots[j]));
                }
                break;
            case 2:
                matrix.rotate(Vector3(0, 0, 1), M_PI / 2);
                for (int i = 0, j = 1; i < FUTARI_CIRCLE_DEFINITION; ++i, ++j) {
                    j %= FUTARI_CIRCLE_DEFINITION;
                    lines.push_back(matrix.xform(circle_dots[i]));
                    lines.push_back(matrix.xform(circle_dots[j]));
                }
                break;
        }
    }

    p_gizmo->add_lines(lines, material);

    //    p_gizmo->add_unscaled_billboard(icon, 0.05);

}

// +++++++++++++++ ATTRACTOR +++++++++++++++

FutariAttractorGizmoPlugin::FutariAttractorGizmoPlugin() {

    Color gizmo_color = EDITOR_DEF(FUTARI_ATTRACTOR_COLOR_NAME, Color(FUTARI_ATTRACTOR_COLOR));
    create_material(FUTARI_ATTRACTOR_MATERIAL, gizmo_color);
    create_handle_material(FUTARI_HANDLE_MATERIAL);

}

bool FutariAttractorGizmoPlugin::has_gizmo(Spatial *p_spatial) {
    return Object::cast_to<FutariAttractor>(p_spatial);
}

String FutariAttractorGizmoPlugin::get_name() const {
    return "FutariAttractor";
}

void FutariAttractorGizmoPlugin::redraw(EditorSpatialGizmo *p_gizmo) {

    FutariAttractor *attractor = Object::cast_to<FutariAttractor>(p_gizmo->get_spatial_node());

    p_gizmo->clear();

    Ref<Material> material;
    if (attractor->get_enabled()) {
        material = get_material(FUTARI_ATTRACTOR_MATERIAL, p_gizmo);
    } else {
        material = get_material(FUTARI_DISABLED_MATERIAL, p_gizmo);
    }
    Ref<Material> handles_material = get_material(FUTARI_HANDLE_MATERIAL);

    Vector<Vector3> arrow_dots;
    float strength = attractor->get_strength();
    curved_arrow(
            attractor->get_range(),
            strength,
            (abs(strength) <= 1) ? 1 : sqrt(abs(strength)),
            M_PI * 0.5,
            arrow_dots);

    int alnum = arrow_dots.size();
    Vector<Vector3> lines;

    Basis tilt_matrix;
    //    tilt_matrix.rotate(Vector3(1, 0, 0), M_PI / 4);
    //    tilt_matrix.rotate(tilt_matrix.xform(Vector3(0, 1, 0)), M_PI / 4);

    for (int r = 0; r < 3; ++r) {
        Basis matrix;
        Vector3 axis(0, 0, 1);
        switch (r) {
            case 1:
                matrix.rotate(Vector3(0, 1, 0), M_PI / 2);
                axis = Vector3(1, 0, 0);
                break;
            case 2:
                matrix.rotate(Vector3(1, 0, 0), M_PI / 2);
                axis = Vector3(0, 1, 0);
                break;
        }
        for (int z = 0; z < 4; ++z) {
            matrix.rotate(axis, M_PI / 2);
            for (int i = 0, j = 1; i < alnum - 1; ++i, ++j) {
                j %= alnum;
                lines.push_back(matrix.xform(arrow_dots[i]));
                lines.push_back(matrix.xform(arrow_dots[j]));
            }
        }
    }

    p_gizmo->add_lines(lines, material);

}

// +++++++++++++++ VORTEX +++++++++++++++

FutariVortexGizmoPlugin::FutariVortexGizmoPlugin() {

    Color gizmo_color = EDITOR_DEF(FUTARI_VORTEX_COLOR_NAME, Color(FUTARI_VORTEX_COLOR));
    create_material(FUTARI_VORTEX_MATERIAL, gizmo_color);
    create_handle_material(FUTARI_HANDLE_MATERIAL);

}

bool FutariVortexGizmoPlugin::has_gizmo(Spatial *p_spatial) {
    return Object::cast_to<FutariVortex>(p_spatial);
}

String FutariVortexGizmoPlugin::get_name() const {
    return "FutariVortex";
}

void FutariVortexGizmoPlugin::redraw(EditorSpatialGizmo *p_gizmo) {

    std::cout << "FutariVortexGizmoPlugin::redraw" << std::endl;

    FutariVortex *vortex = Object::cast_to<FutariVortex>(p_gizmo->get_spatial_node());

    p_gizmo->clear();

    Ref<Material> material;
    if (vortex->get_enabled()) {
        material = get_material(FUTARI_VORTEX_MATERIAL, p_gizmo);
    } else {
        material = get_material(FUTARI_DISABLED_MATERIAL, p_gizmo);
    }
    Ref<Material> handles_material = get_material(FUTARI_HANDLE_MATERIAL);

    real_t rad = vortex->get_range();
    real_t height = vortex->get_height();

    Vector<Vector3> circle_dots;
    circle(rad, circle_dots);

    Vector<Vector3> lines;

    for (int r = 0; r < 2; ++r) {
        Basis matrix;
        matrix.rotate(Vector3(1, 0, 0), M_PI / 2);
        Vector3 add(0, height, 0);
        if (r == 0) {
            add *= -1;
        }
        for (int i = 0, j = 1; i < FUTARI_CIRCLE_DEFINITION; ++i, ++j) {
            j %= FUTARI_CIRCLE_DEFINITION;
            lines.push_back(matrix.xform(circle_dots[i]) + add);
            lines.push_back(matrix.xform(circle_dots[j]) + add);
        }
    }

    real_t a = 0;
    real_t agap = M_PI * 2 / FUTARI_CIRCLE_DEFINITION;
    real_t ygap = 2;
    if (height * 2 * ygap < FUTARI_CIRCLE_DEFINITION * 0.5) {
        ygap = height * 2 * ygap / FUTARI_CIRCLE_DEFINITION * 0.5;
    }

    // double helix
    for (real_t y = -height; y < height; y += ygap) {

        real_t x1 = cos(a) * rad;
        real_t z1 = sin(a) * rad;
        real_t x2 = cos(a + agap) * rad;
        real_t z2 = sin(a + agap) * rad;

        lines.push_back(Vector3(x1, y, z1));
        real_t ny = y + ygap;
        if (ny > height) {
            ny = height;
        }
        lines.push_back(Vector3(x2, ny, z2));

        lines.push_back(Vector3(-x1, y, -z1));
        lines.push_back(Vector3(-x2, ny, -z2));

        a += agap;

    }

    p_gizmo->add_lines(lines, material);

}

// +++++++++++++++ FLOOR +++++++++++++++

FutariFloorGizmoPlugin::FutariFloorGizmoPlugin() {

    Color gizmo_color = EDITOR_DEF(FUTARI_FLOOR_COLOR_NAME, Color(FUTARI_FLOOR_COLOR));
    create_material(FUTARI_FLOOR_MATERIAL, gizmo_color);
    create_handle_material(FUTARI_HANDLE_MATERIAL);

}

bool FutariFloorGizmoPlugin::has_gizmo(Spatial *p_spatial) {
    return Object::cast_to<FutariFloor>(p_spatial);
}

String FutariFloorGizmoPlugin::get_name() const {
    return "FutariFloor";
}

void FutariFloorGizmoPlugin::redraw(EditorSpatialGizmo *p_gizmo) {

    FutariFloor *floor = Object::cast_to<FutariFloor>(p_gizmo->get_spatial_node());

    p_gizmo->clear();

    Ref<Material> material;
    if (floor->get_enabled()) {
        material = get_material(FUTARI_FLOOR_MATERIAL, p_gizmo);
    } else {
        material = get_material(FUTARI_DISABLED_MATERIAL, p_gizmo);
    }
    Ref<Material> handles_material = get_material(FUTARI_HANDLE_MATERIAL);

    Vector<Vector3> lines;
    real_t w = floor->get_width() * 0.5;
    real_t h = floor->get_height() * 0.5;
    for (int i = 0; i < 2; ++i) {
        Vector3 offset(0, 0, 0);
        if (i == 1) {
            offset.y = floor->get_range();
        }
        lines.push_back(Vector3(-w, 0, -h) + offset);
        lines.push_back(Vector3(w, 0, -h) + offset);
        lines.push_back(Vector3(w, 0, -h) + offset);
        lines.push_back(Vector3(w, 0, h) + offset);
        lines.push_back(Vector3(w, 0, h) + offset);
        lines.push_back(Vector3(-w, 0, h) + offset);
        lines.push_back(Vector3(-w, 0, h) + offset);
        lines.push_back(Vector3(-w, 0, -h) + offset);
    }

    p_gizmo->add_lines(lines, material);

}
