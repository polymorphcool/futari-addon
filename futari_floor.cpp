/*
 *  
 *  
 *  _________ ____  .-. _________/ ____ .-. ____ 
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'      
 *  
 *  
 *  art & game engine
 *  
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *  
 * 
 *  This file is part of PROJECT_NAME
 *  For the latest info, see http://polymorph.cool/
 *  
 *  Copyright (c) 2019 polymorph.cool
 *  
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *  
 *  ___________________________________( ^3^)_____________________________________
 *  
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *  
 * 
 */

/* 
 * File:   futari_floor.cpp
 * Author: frankiezafe
 * 
 * Created on March 27, 2019, 2:53 PM
 */

#include "futari_floor.h"

FutariFloor::FutariFloor() :
infinite(false),
width(1.0),
height(1.0),
viscous(0.0),
xaxis(1, 0, 0),
yaxis(0, 1, 0),
zaxis(0, 0, 1) {
    range = FUTARI_FLOOR_MINRANGE;
    FutariFloor::_data.ptr = this;
    FutariFloor::_data.futari_type = FutariModifierData::FMT_FLOOR;
}

void FutariFloor::set_range(real_t r) {
    if (r < FUTARI_FLOOR_MINRANGE) {
        range = FUTARI_FLOOR_MINRANGE;
        return;
    }
    range = r;
#ifdef TOOLS_ENABLED
    update_gizmo();
#endif
}

void FutariFloor::set_infinite(bool b) {
    infinite = b;
#ifdef TOOLS_ENABLED
    update_gizmo();
#endif
}

bool FutariFloor::is_infinite() const {
    return infinite;
}

void FutariFloor::set_width(real_t w) {
    width = abs(w);
#ifdef TOOLS_ENABLED
    update_gizmo();
#endif
}

real_t FutariFloor::get_width() const {
    return width;
}

void FutariFloor::set_height(real_t h) {
    height = abs(h);
#ifdef TOOLS_ENABLED
    update_gizmo();
#endif
}

real_t FutariFloor::get_height() const {
    return height;
}

void FutariFloor::set_viscous(real_t v) {
    viscous = v;
}

real_t FutariFloor::get_viscous() const {
    return viscous;
}

FutariModifierData* FutariFloor::data_ptr() {
    return (FutariModifierData*) &(FutariFloor::_data);
}

void FutariFloor::refresh() {

    FutariFloor::_data.reset();

    if (!enabled) {
        FutariFloor::_data.changed_enabled = FutariFloor::_data.enabled != enabled;
        FutariFloor::_data.changed = FutariFloor::_data.changed_enabled;
        FutariFloor::_data.enabled = enabled;
        return;
    }

    Vector3 p = get_global_transform().origin;
    Vector3 o = get_transform().basis.xform(yaxis);
    Vector3 vx = get_transform().basis.xform(xaxis);
    Vector3 vz = get_transform().basis.xform(zaxis);
    float s = strength;
    if (!is_visible()) {
        s *= 0;
    }

    FutariFloor::_data.changed_layer = FutariFloor::_data.futari_layers != futari_layers;
    FutariFloor::_data.changed_enabled = FutariFloor::_data.enabled != enabled;
    FutariFloor::_data.changed_position = FutariFloor::_data.position != p;
    FutariFloor::_data.changed_orientation = FutariFloor::_data.orientation != o;
    FutariFloor::_data.changed_xaxis = FutariFloor::_data.xaxis != vx;
    FutariFloor::_data.changed_zaxis = FutariFloor::_data.zaxis != vz;
    FutariFloor::_data.changed_strength = FutariFloor::_data.strength != s;
    FutariFloor::_data.changed_strength_decay =
            FutariFloor::_data.strength_decay != strength_decay_texture ||
            FutariFloor::_data.strength_decay.is_valid() != strength_decay_texture.is_valid();
    FutariFloor::_data.changed_lifetime =
            FutariFloor::_data.lifetime != lifetime_texture ||
            FutariFloor::_data.lifetime.is_valid() != lifetime_texture.is_valid();
    FutariFloor::_data.changed_infinite = FutariFloor::_data.infinite != infinite;
    FutariFloor::_data.changed_width = FutariFloor::_data.width != width;
    FutariFloor::_data.changed_height = FutariFloor::_data.height != height;
    FutariFloor::_data.changed_viscous = FutariFloor::_data.viscous != viscous;
    FutariFloor::_data.changed_range = FutariFloor::_data.range != range;
    FutariFloor::_data.changed_velocity_mult = FutariFloor::_data.velocity_mult != velocity_mult;
    FutariFloor::_data.changed_position_mult = FutariFloor::_data.position_mult != position_mult;

    FutariFloor::_data.changed =
            FutariFloor::_data.changed_layer ||
            FutariFloor::_data.changed_enabled ||
            FutariFloor::_data.changed_position ||
            FutariFloor::_data.changed_orientation ||
            FutariFloor::_data.changed_xaxis ||
            FutariFloor::_data.changed_zaxis ||
            FutariFloor::_data.changed_strength ||
            FutariFloor::_data.changed_strength_decay ||
            FutariFloor::_data.changed_lifetime ||
            FutariFloor::_data.changed_infinite ||
            FutariFloor::_data.changed_width ||
            FutariFloor::_data.changed_height ||
            FutariFloor::_data.changed_viscous ||
            FutariFloor::_data.changed_range ||
            FutariFloor::_data.changed_velocity_mult ||
            FutariFloor::_data.changed_position_mult;

    if (FutariFloor::_data.changed) {
        FutariFloor::_data.futari_layers = futari_layers;
        FutariFloor::_data.enabled = enabled;
        FutariFloor::_data.position = p;
        FutariFloor::_data.orientation = o;
        FutariFloor::_data.xaxis = vx;
        FutariFloor::_data.zaxis = vz;
        FutariFloor::_data.strength = s;
        FutariFloor::_data.strength_decay = strength_decay_texture;
        FutariFloor::_data.lifetime = lifetime_texture;
        FutariFloor::_data.infinite = infinite;
        FutariFloor::_data.width = width;
        FutariFloor::_data.height = height;
        FutariFloor::_data.viscous = viscous;
        FutariFloor::_data.range = range;
        FutariFloor::_data.velocity_mult = velocity_mult;
        FutariFloor::_data.position_mult = position_mult;
    }

}

void FutariFloor::_bind_methods() {

    ClassDB::bind_method(D_METHOD("set_infinite", "infinit"), &FutariFloor::set_infinite);
    ClassDB::bind_method(D_METHOD("is_infinite"), &FutariFloor::is_infinite);

    ClassDB::bind_method(D_METHOD("set_width", "width"), &FutariFloor::set_width);
    ClassDB::bind_method(D_METHOD("get_width"), &FutariFloor::get_width);

    ClassDB::bind_method(D_METHOD("set_height", "height"), &FutariFloor::set_height);
    ClassDB::bind_method(D_METHOD("get_height"), &FutariFloor::get_height);

    ClassDB::bind_method(D_METHOD("set_viscous", "viscous"), &FutariFloor::set_viscous);
    ClassDB::bind_method(D_METHOD("get_viscous"), &FutariFloor::get_viscous);

    ADD_GROUP("Floor", "floor_");
    ADD_PROPERTY(PropertyInfo(Variant::BOOL, "enabled"), "set_enabled", "get_enabled");
    ADD_PROPERTY(PropertyInfo(Variant::BOOL, "infinite"), "set_infinite", "is_infinite");
    ADD_PROPERTY(PropertyInfo(Variant::REAL, "width", PROPERTY_HINT_RANGE, "0,100,0.001,or_greater"), "set_width", "get_width");
    ADD_PROPERTY(PropertyInfo(Variant::REAL, "height", PROPERTY_HINT_RANGE, "0,100,0.001,or_greater"), "set_height", "get_height");
    ADD_PROPERTY(PropertyInfo(Variant::REAL, "viscous", PROPERTY_HINT_RANGE, "0,1,0.0001,or_greater"), "set_viscous", "get_viscous");
    String prop = String::num_real(FUTARI_FLOOR_MINRANGE) + ",100,0.01,or_greater";
    ADD_PROPERTY(PropertyInfo(Variant::REAL, "range", PROPERTY_HINT_RANGE, prop), "set_range", "get_range");
    ADD_PROPERTY(PropertyInfo(Variant::REAL, "strength", PROPERTY_HINT_RANGE, "-500,500,or_greater,or_lesser"), "set_strength", "get_strength");
    ADD_PROPERTY(PropertyInfo(Variant::OBJECT, "strength_decay_texture", PROPERTY_HINT_RESOURCE_TYPE, "GradientTexture"), "set_strength_decay_texture", "get_strength_decay_texture");
    ADD_PROPERTY(PropertyInfo(Variant::OBJECT, "lifetime_texture", PROPERTY_HINT_RESOURCE_TYPE, "GradientTexture"), "set_lifetime_texture", "get_lifetime_texture");
    ADD_PROPERTY(PropertyInfo(Variant::REAL, "velocity_mult", PROPERTY_HINT_RANGE, "0,1"), "set_velocity_mult", "get_velocity_mult");
    ADD_PROPERTY(PropertyInfo(Variant::REAL, "position_mult", PROPERTY_HINT_RANGE, "0,1"), "set_position_mult", "get_position_mult");

}